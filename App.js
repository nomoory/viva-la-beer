import React from 'react';
import { StyleSheet, View } from 'react-native';
import { StyleProvider } from 'native-base';
import { getStatusBarHeight } from 'react-native-status-bar-height';

import getTheme from './src/global/styles/native-base-theme/components';
import commonColor from './src/global/styles/native-base-theme/variables/commonColor';

import { Provider } from 'mobx-react';
import stores from './src/stores';

import Navigation from './src/navigation/Navigation';

export default class App extends React.Component {
  render() {
    return (
      <StyleProvider style={ getTheme(commonColor) }>
        <Provider { ...stores }>
          <View style={ styles.container }>
            <View style={ styles.statusBar }/> 
            <Navigation />
          </View>
        </Provider>
      </StyleProvider>
    );
  }
}

const statusBarHeight = getStatusBarHeight();
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },    
  statusBar: {
    zIndex: 1,
    height: statusBarHeight,
    backgroundColor: 'white'
  }
})

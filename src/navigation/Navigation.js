import React from 'react';
import { Text, Button } from 'react-native';
import { 
  createStackNavigator, 
  createSwitchNavigator 
} from 'react-navigation';

import StoreScreen from '../screens/StoreScreen';
import CartScreen from '../screens/CartScreen';

export default createSwitchNavigator(
  {
    Store: StoreScreen,
    Cart: CartScreen
  },
  {
    initialRouteName: 'Store',
  }
);

import React, { Component } from 'react';
import { 
    inject, 
    observer 
} from 'mobx-react';
import { Icon } from 'expo';
import { StyleSheet, View, TouchableHighlight} from 'react-native';

import { Text, Badge } from 'native-base';

@inject('itemStore')
@observer
export default class NavigationBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { totalCountInCart } = this.props.itemStore;
    return (
      <View style={ styles.container }>
        <View style={ styles.headerLeft }>
          <Text style={ styles.title }> 맥주담기 </Text>
        </View>
        <View style={ styles.headerRight }>
          <View style={ styles.buttonContainer }>
            <Icon.Ionicons 
              style={ styles.button }
              focused={ this.props.isStore }
              name="ios-list-outline" 
              color={ this.props.isStore ? 'blue' : 'black' }
              size={ 36 } 
              onPress= { this.props.onPressStore }
            />
          </View>
          <TouchableHighlight style={ styles.buttonContainer }
            onPress={ this.props.onPressCart }>
            <View style={ styles.buttonContainer }>
              <Icon.Ionicons 
                style={ styles.button }
                focused={ this.props.isCart }
                name="ios-cart-outline" 
                color={ this.props.isCart ? 'blue' : 'black' }
                size={ 36 } 
              />
              <Badge style={ styles.badge }>
                <Text style={ styles.bedgeText }>
                  { totalCountInCart }
                </Text>
              </Badge>
            </View>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 60,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: -4,
  },
  headerLeft: {
    padding: 14
  },
  title: {
    fontSize: 24,
    textAlign: 'center'  
  },
  headerRight: {
    flexDirection: 'row',
    height: 60,
    padding: 5,
  },
  buttonContainer: {
    position: 'relative',
    width: 40,
    height: 40,
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  badge: {
    position: 'absolute',
    top: -4,
    right: -3,
    width: 21,
    height: 21
  },
  bedgeText: {
    color: 'white', 
    fontSize: 11,
    lineHeight: 12,
    left: -2
  }
});

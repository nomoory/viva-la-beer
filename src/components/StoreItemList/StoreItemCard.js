import React, { Component } from 'react';
import { 
    inject, 
    observer 
} from 'mobx-react';
import { StyleSheet, View } from 'react-native';
import { Button, Text, Container, Content, Card, CardItem } from 'native-base';
import { Image } from 'react-native';

@inject('itemStore')
@observer
export default class StoreItemCard extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { id, name, image, price, stock, tags, countInCart, countOfTagInCommon } = this.props.item;
    return (
      <View style={ styles.container }>
        <View style={ styles.contents }>
          <Card style={ styles.card }>
            <CardItem>
              <Image
                style={ {width: 60, height: 90} }
                source={{ uri: image }}
              />
              <View style={ styles.descriptions }>
                <Text style={ {fontSize: 20} }>{ name } </Text>
                <View style={ styles.tags }>
                  { tags.map((tag, index) => {
                      let comma = '';
                      if (index < tags.length - 1) {
                        comma = ', ';
                      }
                      return (
                        <Text
                          numberOfLines={1}
                          style={ styles.text }
                          key={`${id}-${tag.key}`}>{ tag.name + comma }</Text>
                      )
                    }
                  )}
                </View>
                <Text style={ styles.text }>
                  <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                    { `${price.toLocaleString('en-us')}` }
                  </Text>
                  {` 원` }
                </Text>
                <View style={ styles.stockAndCountInCart }>
                  <Text style={ styles.text }>{ `재고: ${stock}` } </Text>
                  <Text style={ styles.text }>{ `수량: ${countInCart}` } </Text>
                </View>
              </View>
            </CardItem>
            <CardItem style={ styles.buttons } >
              { this._showItemInButton() }
              { this._showItemOutButton() }
            </CardItem>
          </Card>
        </View>
      </View>
    );
  }
  _showItemInButton = () => {
    const isOutOfStock = this.props.item.stock === 0;
    
    return (          
      <Button small
        style={ styles.button }
        disabled={ isOutOfStock }
        onPress={ this._onPressItemInButton }
      >
        <Text>담기</Text>
      </Button>
    )
  }
  _onPressItemInButton = () => {
    const { id } = this.props.item;
    this.props.itemStore.putItemIntoCart(id);
  }
  _showItemOutButton = () => {
    const noItemInCart = this.props.item.countInCart === 0;
    if (noItemInCart) {
      return null;
    }

    return (
      <Button small light
        style={ styles.button }
        onPress={ this._onPressItemOutButton }
      >
        <Text>빼기</Text>
      </Button>
    )
  }
  _onPressItemOutButton = () => {
    const { id } = this.props.item;
    this.props.itemStore.pullOutItemFromCart(id);
  }

}

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
  },
  card: {
    flex: 1,
    padding: 6,
    backgroundColor: 'white',
  },
  contents: {
    flex: 1,
    flexDirection: 'row',
  },
  descriptions: {
    height: 90,
    paddingLeft: 10,
    justifyContent: 'space-between'
  },
  stockAndCountInCart: {
    flexDirection: 'row',
  },
  tags: {
    flexDirection: 'row',
  },
  buttons: {
    flexDirection: 'row-reverse',
  },
  text: {
    fontSize: 14
  },
  button: {
    marginLeft: 6
  }
});

import React, { Component } from 'react';
import { 
    inject, 
    observer 
} from 'mobx-react';
import { StyleSheet, View, ScrollView } from 'react-native';
import StoreItemCard from './StoreItemCard';
import { Button, Text } from 'native-base';

@inject('itemStore')
@observer
export default class StoreItemList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { items, isLastPage } = this.props.itemStore;
    return (
      <View style={ styles.container }>
        <ScrollView style={ styles.scrollView }>
          { this._showStoreItemCards(items) }
          <View style={ styles.buttonContainer }>
            <Button
              rounded
              disabled={ isLastPage }
              style={ isLastPage ? {} : { backgroundColor: 'white' } }
              onPress={ this._onPressLoadMore }>
              <Text style={isLastPage ? {} : {color: '#95959E', fontWeight: 'bold'}}>더보기 +</Text>
            </Button>
          </View>
        </ScrollView>
      </View>
    );
  }

  _showStoreItemCards = (items) => {
    return items.map((item) => (
      <StoreItemCard key={ item.id } item={ item } />
    ));
  }
  _onPressLoadMore = () => {
    this.props.itemStore.loadItems();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    paddingTop: 0
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
});

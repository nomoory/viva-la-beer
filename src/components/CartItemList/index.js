import React, { Component } from 'react';
import { 
    inject, 
    observer 
} from 'mobx-react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { Text, Button } from 'native-base';
import CartItemCard from './CartItemCard';

@inject('itemStore')
@observer
export default class CartItemList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { itemsInCart, totalCountInCart, totalPriceInCart } = this.props.itemStore;
    return (
      <View style={ styles.container }>
        <ScrollView>
          { this._showCartItemCards(itemsInCart) }
          <View style={ styles.total }>
            <Text style={ styles.totalString }>총 구매수량 <Text style={ styles.totalNumber }>{ totalCountInCart.toLocaleString('en-us') }</Text> 개</Text>
            <Text style={ styles.totalString }>총 결제금액 <Text style={ styles.totalNumber }>{ totalPriceInCart.toLocaleString('en-us') }</Text> 원</Text>
          </View>
          <Button full style={ styles.button } onPress={ this._onPressPurchase }>
            <Text>구매하기</Text>
          </Button>
        </ScrollView>
      </View>
    );
  }

  _showCartItemCards = (items) => {
    return items.map((item) => (
      <CartItemCard key={ item.id } item={ item } />
    ));
  }
  _onPressPurchase = () => {
    this.props.itemStore.purchase();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  total: {
    alignItems: 'flex-end',
    marginBottom: 20,
    marginTop: 20
  },
  totalString: {
    fontSize: 23,
    marginBottom: 5
  }, 
  totalNumber: {
    fontSize: 23,
    fontWeight: 'bold'
  }, 
  button: {
    borderRadius: 5
  }
});

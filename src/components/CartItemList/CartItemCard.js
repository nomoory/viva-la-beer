import React, { Component } from 'react';
import { 
    inject, 
    observer 
} from 'mobx-react';
import { StyleSheet, View } from 'react-native';
import { Button, Text, Container, Content, Card, CardItem } from 'native-base';
import { Image } from 'react-native';

@inject('itemStore')
@observer
export default class StoreItemCard extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { id, name, image, price, stock, tags, countInCart, countOfTagInCommon } = this.props.item;
    return (
      <View style={ styles.container }>
        <View style={ styles.contents }>
          <Card style={ styles.card }>
            <CardItem>
              <Image
                style={ {width: 60, height: 90} }
                source={{ uri: image }}
              />
              <View style={ styles.descriptions }>
                <Text style={ {fontSize: 20} }>{ name }</Text>
                <View style={ styles.tags }>
                  { tags.map((tag, index) => {
                      let comma = '';
                      if (index < tags.length - 1) {
                        comma = ', ';
                      }
                      return (
                        <Text
                          numberOfLines={1}
                          style={ styles.text }
                          key={`${id}-${tag.key}`}>{ tag.name + comma }</Text>
                      )
                    }
                  )}
                </View>
                <Text style={ styles.text }>
                  <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                    { `${price.toLocaleString('en-us')}` }
                  </Text>
                  {` 원` }
                </Text>        
                <Text style={ styles.text }>{ `수량: ${countInCart}` }</Text>
              </View>
            </CardItem>
            <CardItem style={ styles.buttons }>
              { this._showCancelButton() }  
            </CardItem>
          </Card>
        </View>
      </View>
    );
  }
  _showCancelButton = () => {    
    return (          
      <Button small light
        style={ styles.button }
        onPress={ this._onPressCancelButton }
      >
        <Text>취소</Text>
      </Button>
    )
  }
  _onPressCancelButton = () => {
    const { id } = this.props.item;
    this.props.itemStore.cancelItemFromCart(id);
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
  },
  card: {
    flex: 1,
    padding: 6,
    backgroundColor: 'white',
  },
  contents: {
    flexDirection: 'row',
  },
  descriptions: {
    height: 90,
    paddingLeft: 10,
    justifyContent: 'space-between'
  },
  stockAndCountInCart: {
    flexDirection: 'row',
  },
  tags: {
    flexDirection: 'row',
  },
  buttons: {
    flexDirection: 'row-reverse',
  },
  button: {
    marginLeft: 6
  }
});

import React, { Component } from 'react';
import { 
    inject, 
    observer 
} from 'mobx-react';
import { Icon } from 'expo';
import { StyleSheet, View } from 'react-native';
import { Button, Content, Text } from 'native-base';

export default class EmptyCart extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={ styles.container }>
        <Icon.Ionicons 
          style={ styles.button }
          name="ios-basket-outline" 
          color={ 'grey' }
          size={ 120 } 
        />
        <Text style={ { fontSize: 24, fontWeight: 'bold', marginBottom: 12 } }>카트가 비었습니다</Text>
        <Text>{ `목록에 원하는 맥주를` }</Text>
        <Text>{ `카트에 담아보세요` }</Text>
        <Content style={ styles.buttonContainer }>
          <Button small
            style={ styles.button }
            onPress={ this._onPressGoToStore }>
            <Text style={ styles.buttonText }>목록으로 가기</Text>
          </Button>
        </Content>
      </View>
    );
  }
  _onPressGoToStore= () => {
    this.props.navigation.navigate('Store');
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 100
  },
  buttonContainer: {
    marginTop: 20
  },
  buttonText: {
    marginLeft: 30,
    marginRight: 30,
  }
});

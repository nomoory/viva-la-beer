import React, { Component } from 'react';
import { 
    inject, 
    observer 
} from 'mobx-react';
import { Icon } from 'expo';
import { StyleSheet, View, ScrollView } from 'react-native';
import Tag from './Tag';

@inject('tagStore')
@observer
export default class TagBox extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { tags } = this.props.tagStore;
    return (
      <View style={ styles.container }>
        <ScrollView
          style={ styles.scrollContainer }
          horizontal={ true }
          showsHorizontalScrollIndicator={ false }
        >
          { this._showTags(tags) }
        </ScrollView>
      </View>
    );
  }

  _showTags = (tags) => {
    return tags.map((tag) => (
      <Tag key={ tag.key } tag={ tag } />
    ));
  }
}

const styles = StyleSheet.create({
  container: {
    height: 60,
    backgroundColor: '#00000000',
  },
  scrollContainer: {
    padding: 10
  }
});

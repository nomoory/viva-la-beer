import React, { Component } from 'react';
import { 
    inject, 
    observer 
} from 'mobx-react';
import { Icon } from 'expo';
import { StyleSheet, View } from 'react-native';
import { Button, Text, Container } from 'native-base';

@inject('tagStore')
@observer
export default class Tag extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { name, selected } = this.props.tag;
    return (
      <View style={ styles.container }>
        <Button 
          small
          style={ styles.button }
          bordered={ !selected }
          onPress={ this._selectTag }>
          <Text>{ name }</Text>
        </Button>
      </View>
    );
  }
  _selectTag = () => {
    this.props.tagStore.toggleTag(this.props.tag.key);
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#00000000',
  },
  button: {
    margin: 5
  },
});

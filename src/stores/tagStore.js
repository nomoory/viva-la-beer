import {
  observable,
  computed,
  action
} from 'mobx';

import api from '../utils/api';

class TagStore {
  @observable inProgress = false;
  @observable errors = null;

  @observable tagsRegistry = observable.map();
  @computed get count() {
    return this.tagsRegistry.size;
  };

  @computed get tags() {
    let tags = [];
    this.tagsRegistry.forEach((tag, key) => {
      tags.push(tag);
    });
    return tags;
  }

  getTagByTagKey(tagKey) {
    return this.tagsRegistry.get(tagKey);
  }
  
  @computed get selectedTags() {
    let tags = [];
    this.tagsRegistry.forEach((tag) => {
      if (tag.selected) {
        tags.push(tag);
      }
    });
    return tags;
  }

  @action toggleTag(tagKey) {
    const tag = this.getTagByTagKey(tagKey);
    tag.selected = !tag.selected;
  }

  loadTags() {
    this.inProgress = true;
    this.errors = undefined;
    api.getTags().then(action((response) => {
      let tags = response.data || [];
      this.tagsRegistry.clear();
      tags.forEach((tag) => {
          this.tagsRegistry.set(tag.key, {...tag, selected: true});
      });
    })).catch(action((error) => {
      this.errors = error;
    }))
    .then(action(() => {
        this.inProgress = false;
    }));
  }
}

const tagStore = new TagStore();

export default tagStore;
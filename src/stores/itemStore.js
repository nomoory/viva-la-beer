import {
  observable,
  computed,
  action,
  reaction
} from 'mobx';

import tagStore from './tagStore';
import api from '../utils/api';

class ItemStore {
  @observable inProgress = false;
  @observable error = null;
  @observable page = 1;
  @observable data = false;

  @observable itemsRegistry = observable.map();

  constructor() {
    const selecetedTagsReaction = reaction(
      () => tagStore.selectedTags,
      (selectedTags) => {
        this.itemsRegistry.forEach((item) => {  
          this._setCountOfTagInCommonInItem(item, selectedTags);
        })
      }
    );
  }

  @computed get items() {
    let items = [];
    const selectedTags = tagStore.selectedTags;

    this.itemsRegistry.forEach((item) => {
      items.push(this._setCountOfTagInCommonInItem(item, selectedTags));
    });
    items = this._filter(items);
    items = this._sort(items);
    return items;
  }

  @computed get itemsInCart() {
    let items = [];
    this.itemsRegistry.forEach((item) => {
      if (item.countInCart) {
        items.push(item);
      }
    });
    return items;
  }

  @computed get totalCountInCart() {
    let totalCount = 0;
    this.itemsRegistry.forEach((item) => {
      totalCount += item.countInCart;
    });
    return totalCount;
  }

  @computed get totalPriceInCart() {
    let totalPrice = 0;
    this.itemsRegistry.forEach((item) => {
      totalPrice += item.price * item.countInCart;
    });
    return totalPrice;
  }

  _setCountOfTagInCommonInItem(item, selectedTags) {
    let itemWithCountOfTagInCommon = { 
      ...item,
      countOfTagInCommon: this._getCountOfTagInCommon(item, selectedTags)
    };
    return itemWithCountOfTagInCommon;
  }

  _getCountOfTagInCommon(item, selectedTags) {
    let count = 0;
    const tagsInItem = item.tags || [];
    for (let tagInItem of tagsInItem) {
      for (let selectedTag of selectedTags) {
        if (tagInItem.key === selectedTag.key) {
          count += 1;
        }
      }
    }
    return count;
  }

  _filter(items) {
    const filteredItem = items.filter((item) => item.countOfTagInCommon !== 0);    
    return filteredItem;
  }
  _sort(items) {
    const sortedItem = items.sort((item1, item2) => item1.countOfTagInCommon < item2.countOfTagInCommon );    
    return sortedItem;
  }

  getItemById(itemId) {
    return this.itemsRegistry.get(itemId);
  }

  @action putItemIntoCart(itemId) {
    const item = this.getItemById(itemId);
    if (0 < item.stock) {
      this.itemsRegistry.set(itemId, {...item,
        stock: item.stock - 1,
        countInCart: item.countInCart + 1
      })
    } else {
      alert('재고가 없습니다.');
    }
  }
  @action pullOutItemFromCart(itemId) {
    const item = this.getItemById(itemId);
    if (0 < item.countInCart) {
      this.itemsRegistry.set(itemId, { ...item,
        stock: item.stock + 1,
        countInCart: item.countInCart - 1
      })
    } else {
      alert('장바구니에 상품이 없습니다.');
    }
  }
  @action cancelItemFromCart(itemId) {
    const item = this.getItemById(itemId);
    this.itemsRegistry.set(itemId, { ...item,
      stock: item.stock + item.countInCart,
      countInCart: 0
    })
  }
  @action reloadItems() {
    this.inProgress = false;
    this.page = 1;
    this.itemsRegistry.clear();
    this.loadItems();
  }

  loadItems() {
    if (this.inProgress) {
      alert('요청이 진행중입니다.');
      return;
    };
    this.inProgress = true;
    this.error = undefined;

    api.getItems(this.page).then(action((response) => {
      const items = response.data.items || [];      
      items.forEach((item) => {
          this.itemsRegistry.set(item.id, {
            ...item,
            // cart에 들어있는 물품 개수를 초기화합니다.
            countInCart: 0,            
            // 몇 개의 tag가 filter에 걸리는지 나타내는 변수를 초기화합니다.
            countOfTagInCommon: 0 
          });
      });
      this.page = this.page + 1;
      this.isLastPage = response.data.isLastPage;
    })).catch(action((error) => {
      this.error = error;
    }))
    .then(action(() => {
        this.inProgress = false;
    }));
  }

  purchase() {
    this.inProgress = true;
    this.error = undefined;

    const orders = this._getOrders();

    api.purchase(orders).then((res)=>{
      let totalCount = res.totalCount || this.totalCountInCart;
      let totalPrice = res.totalPrice || this.totalPriceInCart;
      alert(`주문을 마쳤습니다.\n총 구매수량: ${ totalCount }\n총 결제금액: ${ totalPrice }`);
      this.reloadItems();
    }).catch(action((error) => {
      this.error = error;
      alert(`${error.title}: ${error.reason}`);
    })).then(action(() => {
      this.inProgress = false;
    }));
  }

  _getOrders = () => {
    let orders = [];
    this.itemsInCart.forEach((item) => {
      let order = {
        id: item.id,
        count: item.countInCart
      };
      orders.push(order);
    });
    return orders;
  }

}

const itemStore = new ItemStore();

export default itemStore;
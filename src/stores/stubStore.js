import {
  observable,
  computed,
  action,
  reaction
} from 'mobx';

import api from '../utils/api';

class StubStore {
  @observable count = 5;
  @observable price = 1000;
  @computed get totalPriceWithUnit() {
    return `${ this.count * this.price } 원`;
  }

  constructor() {
    const countReaction = reaction(
      () => this.count,
      (value) => {
        alert(
          '추가 성공!',
          `Successfully added!! (${value})`
        )
      }
    )
  }

  addWithServerData() {
    api.getIncrease().then(
      (response) => {
        let increase = response.data && response.data[0] && response.data[0].increase;
        this.addCount(increase);
      }
    )
  }

  @action addCount(increase = 1) {
    this.count = this.count + increase;
  }

  @action minusCount(decrease = 1) {
    this.count = this.count - decrease;
  }
}

const stubStore = new StubStore();

export default stubStore;
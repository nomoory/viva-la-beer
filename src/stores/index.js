import itemStore from './itemStore';
import stubStore from './stubStore';
import tagStore from './tagStore';

const stores = {
    itemStore,
    stubStore,
    tagStore,
};

export default stores;
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { StyleSheet } from 'react-native';
import { Container, Button, Text } from 'native-base';

@inject('stubStore')
@observer
export default class TestScreen extends Component {
  render() {
    return (
      <Container style={styles.container}>
        <Text>{ `you have ${ this.props.stubStore.count } beer(s)` }</Text>
        <Button onPress={ () => { this.props.stubStore.addCount() } }>
          <Text>
            { `click here to add more` }
          </Text>
        </Button>
        <Button onPress={ () => { this.props.stubStore.addWithServerData() } }>
          <Text>
            { `click here to add more(async)` }
          </Text>
        </Button>

        <Text>{ `you have ${ this.props.stubStore.totalPriceWithUnit } beer(s)` }</Text>
      </Container>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});




import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { StyleSheet, View } from 'react-native';
import { Container, Button, Text, Content } from 'native-base';
import NavigationBar from '../components/NavigationBar';
import TagBox from '../components/TagBox';
import StoreItemList from '../components/StoreItemList';

@inject('tagStore', 'itemStore')
@observer
export default class StoreScreen extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
  }

  async componentDidMount(){
    await this.props.tagStore.loadTags();
    await this.props.itemStore.reloadItems();
  }

  render() {
    return (
      <Container style={ styles.container }>
        <NavigationBar 
          isStore={ true }
          isCart={ false }
          onPressStore={ null }
          onPressCart={ this._onPressCart }
        />
        <TagBox />
        <Content style={ styles.content }>
          <StoreItemList />
        </Content>
      </Container>
    )
  }

  _onPressCart = (e) => {
    this.props.navigation.navigate('Cart');
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F2F3F7'
  },
  content: {
    flex: 1
  },
  buttons: {        
    width: 100,
    flexDirection: 'row',
  },
  button: {
    flex:1,
    alignSelf: 'center',
    fontSize: 34,
    textAlign: "center",
  }
});




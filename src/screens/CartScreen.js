import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { StyleSheet, View } from 'react-native';
import { Container, Content, Text } from 'native-base';
import NavigationBar from '../components/NavigationBar';
import EmptyCart from '../components/EmptyCart';
import CartItemList from '../components/CartItemList';
@inject('itemStore')
@observer
export default class CartScreen extends Component {
  render() {
    return (
      <Container style={ styles.container }>
          <NavigationBar 
            isStore={ false }
            isCart={ true }
            onPressStore={ this._onPressStore }
            onPressCart={ null }
          />
        <Content>
          {
            this.props.itemStore.totalCountInCart == 0 ?
            <EmptyCart navigation={ this.props.navigation }></EmptyCart> :
            <CartItemList></CartItemList>
          }
        </Content>
      </Container>
    )
  }
  _onPressStore = (e) => {
    this.props.navigation.navigate('Store');
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F2F3F7'
  },
});




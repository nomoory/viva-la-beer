export default {
  increases: [{
    id: 'son',
    increase: '2'
  }],
  // stock 은 stubApi에서 랜덤 분배
  items: [
    {
      id: 1,
      name: 'Hitachino',
      image: 'https://i.pinimg.com/originals/9e/ca/92/9eca92eafdcba7746b203a0499a5b540.jpg',
      tags: [
        {
          key: '1',
          name: '라거',
        },{
          key: '4',
          name: '국산맥주',
        },{
          key: '5',
          name: '수입맥주',
        },{
          key: '6',
          name: 'OB맥주',
        }
      ],
      price: 5000,
    },
    {
      id: 2,
      name: 'Kingfisher',
      image: 'https://i.pinimg.com/originals/f7/b5/cb/f7b5cbe76cf51ee0aec33e42b2ad9668.jpg',
      tags: [
        {
          key: '1',
          name: '라거',
        },{
          key: '2',
          name: '에일',
        },{
          key: '3',
          name: '람빅',
        },
      ],
      price: 5000,
    },
    {
      id: 3,
      name: 'Hacket-Pschorr',
      image: 'https://i.pinimg.com/originals/27/53/5a/27535a927392cb49b4bf9343342dcc74.jpg',
      tags: [
        {
          key: '1',
          name: '라거',
        },
      ],
      price: 6000,
    },
    {
      id: 4,
      name: 'Piraat',
      image: 'https://i.pinimg.com/originals/39/7a/62/397a62da92f10a9dc41bf75ad1f57a08.jpg',
      tags: [
        {
          key: '4',
          name: '국산맥주',
        },{
          key: '5',
          name: '수입맥주',
        },{
          key: '6',
          name: 'OB맥주',
        }
      ],
      price: 8000,
    },
    {
      id: 5,
      name: 'Bornem',
      image: 'https://i.pinimg.com/originals/a6/c2/00/a6c2008b36d7ac7d1891ab6f386eb132.jpg',
      tags: [
        {
          key: '2',
          name: '에일',
        },{
          key: '3',
          name: '람빅',
        },
      ],
      price: 8000,
    },
    {
      id: 6,
      name: 'Pipeline',
      image: 'https://i.pinimg.com/originals/fd/f3/51/fdf351a7c50961b3a98d47ba059ded5b.jpg',
      tags: [
        {
          key: '1',
          name: '라거',
        },
      ],
      price: 7000,
    },
    {
      id: 7,
      name: 'Trappistes Rochefort',
      image: 'http://i.pinimg.com/736x/d6/90/60/d69060bd1cc8fe7a789ac853a9568c36.jpg',
      tags: [
        {
          key: '1',
          name: '라거',
        },{
          key: '2',
          name: '에일',
        },{
          key: '3',
          name: '람빅',
        },{
          key: '4',
          name: '국산맥주',
        }
      ],
      price: 6000,
    },
    {
      id: 8,
      name: 'Huber Bock',
      image: 'https://i.pinimg.com/originals/62/8d/aa/628daacf7e4728b004ef94309649d9c8.jpg',
      tags: [
        {
          key: '2',
          name: '에일',
        },{
          key: '3',
          name: '람빅',
        },{
          key: '4',
          name: '국산맥주',
        }
      ],
      price: 7000,
    },
  ],
  tags: [
    {
      key: '1',
      name: '라거',
    },{
      key: '2',
      name: '에일',
    },{
      key: '3',
      name: '람빅',
    },{
      key: '4',
      name: '국산맥주',
    },{
      key: '5',
      name: '수입맥주',
    },{
      key: '6',
      name: 'OB맥주',
    }
  ]
};

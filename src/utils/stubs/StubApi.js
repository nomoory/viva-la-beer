import axios from 'axios';
import { AsyncStorage } from 'react-native';
import stubData from './stubData';

export default class StubAPI {
  constructor(baseURL = null) {
    this.axios = axios.create({ baseURL });
  }

  /* APIs */

  // example
  getSomthingWithDelay() {
    return this._delay(stubData.increases);
  }

  getIncrease() {
    return this._delay(stubData.increases);
  }

  getItems(page = 1) { // paging을 위한 page 변수이며 1부터 시작합니다.
    let isLastPage = stubData.items.length < page * 5;
    let itemsInCurrentPage = stubData.items.slice((page - 1) * 5, page * 5);
    let itemsWithStockValue = this._setStockOfItems(itemsInCurrentPage);
    let data = {
      isLastPage,
      items: itemsWithStockValue
    }
    return this._delay(data);
  }

  _setStockOfItems(items) {
    const itemsWithStock = [];
    items.forEach((item) => {
      itemsWithStock.push({
        ...item,
        stock: Math.floor(Math.random() * 10 + 1)
      })
    });
    return itemsWithStock;
  }

  getTags() {
    return this._delay(stubData.tags);
  }

  purchase(order) {
    let error = {
      title: '서버에러' ,
      reason: '알수 없는 에러'
    };
    return this._delay()
  }

  /* Base REST API method */
  get(url) {
    return this.axios
      .get(url, this._getRequestConfig())
      .catch(this._handleError);
  }
  put(url, body) {
    return this.axios
      .put(url, body, this._getRequestConfig())
      .catch(this._handleError);
  }
  post(url, body) {
    return this.axios
      .post(url, body, this._getRequestConfig())
      .catch(this._handleError);
  }
  delete(url) {
    return this.axios
      .delete(url, this._getRequestConfig())
      .catch(this._handleError);
  }
  _getRequestConfig() {
    let requestConfig = null;
    let accessToken = AsyncStorage.getItem('accessToken');
    if (accessToken) {
      requestConfig = { headers: { 'Authorization': `Bearer ${accessToken}` }};
    }
    return requestConfig;
  }

  _handleError(error) {
    // status code에 따른 처리
    if (error && error.response && error.response.status === 401) {
    }
    throw error;
  }

  /* 가상의 asyncgks 요청을 생성합니다. 
   * time(ms) 시간 후에 응답이 옵니다. 
   * promise then을 통해 response.data로 가상의 응답 데이터(stubData)를 조회합니다. 
   */

  _delay(data = {}, time = 500) {
    return new Promise(function(resolve) { 
        setTimeout(() => {
          let response = { data };
          resolve(response);
        }, time);
    });
  }
}

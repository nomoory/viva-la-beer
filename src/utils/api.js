import axios from 'axios';
import { AsyncStorage } from 'react-native';
import StubApi from './stubs/StubApi';

const DEV_API_ENDPOINT = Expo.Constants.manifest.extra.DEV_API_ENDPOINT;
const STAGING_API_ENDPOINT = Expo.Constants.manifest.extra.STAGING_API_ENDPOINT;

class API {
  constructor(baseURL = null) {
    this.axios = axios.create({ baseURL });
  }

  /* APIs */
  // example with auth
  signup(payload) {
    return this.post(`signup/`, payload);
  }

  login(id, password) {
    return this.post(`users/login/`, {
      id,
      password
    });
  }

  logout() {
    return this.delete(`users/logout/`);
  }

  /* Base REST API method */
  get(url) {
    return this.axios
      .get(url, this._getRequestConfig())
      .catch(this._handleError);
  }
  put(url, body) {
    return this.axios
      .put(url, body, this._getRequestConfig())
      .catch(this._handleError);
  }
  post(url, body) {
    return this.axios
      .post(url, body, this._getRequestConfig())
      .catch(this._handleError);
  }
  delete(url) {
    return this.axios
      .delete(url, this._getRequestConfig())
      .catch(this._handleError);
  }
  _getRequestConfig() {
    let requestConfig = null;
    let accessToken = AsyncStorage.getItem('accessToken');
    if (accessToken) {
      requestConfig = { headers: { 'Authorization': `Bearer ${accessToken}` }};
    }
    return requestConfig;
  }
  _handleError(error) {
    // status code에 따른 처리를 합니다.
    if (error && error.response && error.response.status === 401) {
    }
    throw error;
  }
}

let api;
if (__DEV__ ) { // __DEV__ 는 react-native 에서 테스트 환경을 구분하는 인자로 사용됩니다.
  const API_ROOT = `${DEV_API_ENDPOINT}/`;
  api = new StubApi(API_ROOT);
} else {
  const API_ROOT = `${STAGING_API_ENDPOINT}/`;
  api = new API(API_ROOT);
}

export default api;
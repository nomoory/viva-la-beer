# viva-la-beer

Small toy project with react-native and expo.


# react native로 개발 이유
제시된 homework의 UI가 모바일에서 보는 형태였기에 모바일 환경에서 사용하는 것이라고 가정했습니다.
앱으로 제작하면 제작하는 과정에서 스마트 폰으로 테스트가 가능하여, 구현한 기능 확인과 UX나 UI를 개선이 용이하기에 react native 환경을 세팅하여 개발하였습니다.

# 개발 환경 설치 방법

1) git에서 본 코드를 내려받습니다.

2) npm, node 인스톨 합니다. 

(개발환경의 version: node v8.11.4 / npm v6.4.0)

3) command line tool install

$ npm install expo-cli --global

4) expo XDE download

project를 쉽게 build하고 test할 수 있게 해주는 runner 입니다.

https://docs.expo.io/versions/v30.0.0/introduction/xde-tour

*계정이 필요합니다.

5) 스마트폰에 expo client install

expo client에서 react native로 제작한 코드의 동작을 실제 앱과 동일하게 확인 가능합니다. (프로젝트를 실행한 계정과 동일 계정으로 로그인이 필요합니다)

- android app: https://play.google.com/store/apps/details?id=host.exp.exponent&referrer=www

- ios app: https://itunes.apple.com/app/apple-store/id982107779

# 실행 방법

1) project를 받아 npm istall을 실행합니다.

2) expo XDE를 실행합니다.

3) expo XDE에서 open existing project를 프로젝트를 오픈합니다.

4) 스마트폰에서 expo client app을 실행하면 현재 오픈한 앱이 뜨고 실행이 가능합니다.

